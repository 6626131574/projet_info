# importation des modules utiles au programme
from tkinter import Tk, Canvas, LEFT, Frame, RIGHT, Button
import numpy as np


""" ClIC DROIT --> poser un point
    CLIC GAUCHE sur 1 point puis un autre point --> relier 2 points """

""" REMARQUE 1 : Chaque objet posé a un numéro associé. Pour relier les points
sans avoir à récupérer les coordonnées d'un point qu'on touche,
on pourrait faire afficher le numéro du point à sa création, puis entrer dans
une console les deux points à relier ... Ce serait moins coûteux en recherche.
Avec énormément de points, la numérotation est mieux,
avec peu de point ce n'est pas très grave.
Sachant qu on ne va pas travailler avec 100 points je garde la premiere option,
plus légère visuellement """

""" les numéros des sommets commencent à 1,
le 0 est un ajout pour simplifier le programme """

""" Remarque 2 : On peut récupérer : - la liste des voisins
                                     - la matrice d'adjacence en les faisant
afficher dans la console, en utilisant les boutons de la fenêtre graphique """


class Fenetre:
    def __init__(self):

        # création de la fenetre
        self.largeur = 800
        self.hauteur = 800
        self.fenetre = Tk()
        self.fenetre.title("Création d'un graphe")

        # création du canvas et de la partie bouton
        self.cadre = Canvas(self.fenetre, width=self.largeur,
                            height=self.hauteur, bg='grey')
        self.cadre.pack(side=LEFT)

        self.cadrebouton = Frame(
            self.fenetre, width=self.largeur, height=self.hauteur, bg='white')
        self.cadrebouton.pack(side=RIGHT)

        # objets nécessaires pour les différents programmes self.
        self.nombre_de_points = 0
        self.liste_coord_grille = [[0, 0]]
        self.matrice_adjacence = np.array([[0]])
        self.liste_voisin = [[]]

        # pour la fonction relier_points
        self.t = 0
        self.point_1 = 0
        self.point_2 = 0

        # fonction 1 : récupérer des coordonnées
        # d'un point suite à un évènement
        def recuperer_coord(event):
            x = event.x
            y = event.y
            return x, y

        # fonction 2 : poser les points, enregistrer les coordonnées
        def poser_points(event):
            x, y = recuperer_coord(event)
            self.liste_coord_grille.append((x, y))
            self.nombre_de_points += 1
            self.cadre.create_oval(x-10, y-10, x+10, y+10, fill='black')
            self.matrice_adjacence = np.insert(
                self.matrice_adjacence, self.nombre_de_points, 0, axis=0)
            # ajoute une ligne
            self.matrice_adjacence = np.insert(
                self.matrice_adjacence, self.nombre_de_points, 0, axis=1)
            self.liste_voisin.append([])
        # fonction 4 : tracer une droite entre 2 points
        """ J'introduit des variables nécessaires pour tracer les droites,
                c'est à dire :
                t --> pour savoir si on a déjà cliqué sur un point
                point_1 / 2 --> pour récupérer le numéro des objets à lier """
        def relier_points(event):
            x, y = recuperer_coord(event)
            if self.t == 0:  # il n'y a pas de point déjà enregistré
                # on récupère le numéro de l'objet
                # ou 0 si ce n'est pas un objet
                self.point_1 = self.numero_objet(x, y)
                if self.point_1 != 0:
                    self.t += 1
            if self.t == 1:
                self.point_2 = self.numero_objet(x, y)
                if self.point_2 != 0 and self.point_2 != self.point_1:
                    # pour ne pas lier un point avec lui même
                    x1 = self.liste_coord_grille[self.point_1][0]
                    x2 = self.liste_coord_grille[self.point_2][0]
                    y1 = self.liste_coord_grille[self.point_1][1]
                    y2 = self.liste_coord_grille[self.point_2][1]
                    self.cadre.create_line(x1, y1, x2, y2)
                    self.t = 0   # on remet le compteur à 0
                    self.matrice_adjacence[self.point_1][self.point_2] = 1
                    self.matrice_adjacence[self.point_2][self.point_1] = 1
                    self.liste_voisin[self.point_1].append(self.point_2)
                    self.liste_voisin[self.point_2].append(self.point_1)

        # pour poser un point clic droit et pour relier deux points,
        # clic molette
        self.cadre.bind("<Button-1>", poser_points)
        self.cadre.bind("<Button-2>", relier_points)

        self.Bouton1 = Button(
            self.cadrebouton, text='quitter', command=self.fenetre.destroy)
        self.Bouton1.pack()
        self.Bouton2 = Button(
            self.cadrebouton, text='Floyd Warshall',
            command=self.FloydWarshallConnexe)
        self.Bouton2.pack()
        self.fenetre.mainloop()

    # fonction 3 : chaque objet est numéroté par ordre de création,
    # on peut le manipuler en connaissant ce nombre

    def numero_objet(self, x, y):
        for i in range(len(self.liste_coord_grille)):
            [a, b] = self.liste_coord_grille[i]
            if x <= (a+10) and x >= (a-10):
                if y <= (b+10) and y >= (b-10):
                    return i
                    # et pas i car on numérote en partant de 1
                    # et la liste part de 0
        return 0

    # fonction 5 : Floyd Warshall

    def FloydWarshallConnexe(self):  # on entre la matrice d'adjacence
        n = self.nombre_de_points  # nombre de sommets
        en_etude = []

        for i in range(1, n):
            # on regarde combien il y a de 0. Complexité en n(n-1)/2,
            # partie triangulaire supérieure de la matrice sans diagonale
            for j in range(i+1, n+1):
                if self.matrice_adjacence[i][j] == 0:
                    en_etude.append((i, j))
        puissance = 1
        print("\nMatrice d'adjacence du graphe :")
        print(self.matrice_adjacence)
        print(en_etude, 'initial')
        # à la place de visiter la matrice
        # on ne va visiter que la liste des chaines non existentes encore
        while en_etude != [] and puissance < n:
            m = len(en_etude)
            nombre_supprime = 0
            # car si on en supprime il y a un changement d indice pour la suite
            for idx in range(m):
                (x, y) = en_etude[idx-nombre_supprime]
                # ça veit dire que la chaine existe
                if ((x, puissance)not in en_etude and (puissance, x)
                        not in en_etude) and ((y, puissance) not in en_etude
                                              and (puissance, y)
                                              not in en_etude):
                    del en_etude[idx-nombre_supprime]
                    nombre_supprime += 1
                # le sinon est : la chaine n'existe pas donc
                # on laisse le couple dans la liste
            puissance += 1

        print(en_etude, 'final')
        if en_etude == []:
            print("Le graphe est connexe.\n")
            return("Le graphe est connexe.")
        print("Le graphe n'est pas connexe.\n")
        return("Le graphe n'est pas connexe.")
