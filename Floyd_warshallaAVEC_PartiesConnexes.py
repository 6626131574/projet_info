#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 11 21:36:41 2019

@author: cecilepetitet
"""
import Algo


def FloydWarshallPartiesConnexe(M):  # on entre la matrice d'adjacence
    n = len(M[0])  # nombre de sommets
    puissance = 0
    matrice = M
    reponse = False
    while not reponse and puissance < n:
        # à la place de visiter la matrice
        # on ne va visiter que la liste des chaines non existentes encore
        for i in range(n-1):
            for j in range(i+1, n):
                if M[i][j] == 0:
                    if M[i][puissance] == 1 and M[puissance][j] == 1:
                        M[i][j] = M[j][i] = 1
        puissance += 1
        if matrice == [[1 for _ in range(n)] for _ in range(n)]:
            reponse = True

    if reponse:
        return (1)

    else:
        sommets_visite = []
        parties_connexes = []
        for i in range(n):
            if i not in sommets_visite:
                part_i = []
                for idx, elt in enumerate(matrice[i]):
                    if elt == 1:
                        part_i.append(idx)
                        sommets_visite.append(idx)
                parties_connexes.append(part_i)
        return(len(parties_connexes))


def test():
    print("Test de l'algo FloydWarshallPartiesConnexe")
    M = [
        [1, 1, 0, 1, 0],
        [1, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [1, 0, 0, 1, 0],
        [0, 0, 0, 0, 1]]
    Algo.afficher_matrice(M)
    print("Attendu : 0. Nombre de partie connexe :",
          FloydWarshallPartiesConnexe(M))

    M = [
        [1, 1, 0, 1, 0],
        [1, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [1, 0, 0, 1, 1],
        [0, 0, 0, 1, 1]]
    Algo.afficher_matrice(M)
    print("Attendu : 1. Nombre de partie connexe :",
          FloydWarshallPartiesConnexe(M))
