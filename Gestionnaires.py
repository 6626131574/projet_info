from random import randint, random
from Floyd_warshallaAVEC_PartiesConnexes import FloydWarshallPartiesConnexe
import Algo
from time import time
from math import floor, log, exp
import os
import numpy as np
import matplotlib.pyplot as plt


# classe la plus efficace actuelle : GestV2NumpyConex2

def lire_parametre():
    print("Combien de graphes ?,Combien de sommets ? (ex:24,37)")
    texte = input()
    p1 = ""
    p2 = ""
    token = 1
    for c in texte:
        if c != "," and token == 1:
            p1 += c
        elif c != "," and token == 2:
            p2 += c
        else:
            token += 1
    try:
        p1 = int(p1)
        p2 = int(p2)
        return p1, p2
    except ValueError:
        print("Mauvaise syntaxe.\nOpération intérompu.\n")
        return -1,-1


class CoupleGraphePartiesConnexes:
    def __init__(self, matrice, parties_connexes):
        self.matrice = matrice
        self.parties_connexes = parties_connexes


class UpletPourGraphe:
    def __init__(self, matrice, parties_connexes,
                classe_equivalence, nb_partie_connexe):
        self.matrice = matrice
        self.parties_connexes = parties_connexes  # la liste des représentants
        self.classe_equivalence = classe_equivalence
        self.nb_partie_connexe = nb_partie_connexe


class GestV2:

    def __init__(self, nb_graphes, nb_sommets):
        self.nb_graphes = nb_graphes
        self.nb_sommets = nb_sommets
        self.nb_darrete_graphes = 0
        self.max_arrete = nb_sommets*(nb_sommets-1)//2
        # [[matrice d'adjacence,liste des parties connexes],...] couple
        self.liste_des_graphes = [CoupleGraphePartiesConnexes(
            [[0 for i in range(nb_sommets)] for j in range(nb_sommets)],
            [[i] for i in range(nb_sommets)]) for k in range(nb_graphes)]

    def reset(self):
        if self.nb_darrete_graphes != 0 :
            self.nb_darrete_graphes = 0
            self.liste_des_graphes = [CoupleGraphePartiesConnexes(
                [[0 for i in range(self.nb_sommets)] for j
                in range(self.nb_sommets)],
                [[i] for i in range(self.nb_sommets)]) for k
                in range(self.nb_graphes)]


    def trouver_index_connexe(self, S, liste_des_parties_connexe):
        for i in range(len(liste_des_parties_connexe)):
            for sommet in liste_des_parties_connexe[i]:
                if sommet == S:
                    return i

    def fusionner_les_parties_connexes(self, i, j, liste_des_parties_connexe):
        if i != j:
            liste_des_parties_connexe[i] = liste_des_parties_connexe[i] + \
                liste_des_parties_connexe[j]
            del liste_des_parties_connexe[j]

    # matrice d adjacence et liste des parties connexes
    def __ajouter_une_arrete_aleatoire(self, matrice_adjacence,
                                       liste_des_parties_connexes):
        limite = self.nb_sommets
        if limite != 0:
            fini = False
            M = matrice_adjacence
            # rempli une case de la matrice (ajoute une arrête
            while not fini:
                x = floor(random()*limite)
                y = floor(random()*limite)
                if M[x][y] == 0 and x != y:
                    M[x][y] = M[y][x] = 1
                    c1 = self.trouver_index_connexe(
                        x, liste_des_parties_connexes)
                    c2 = self.trouver_index_connexe(
                        y, liste_des_parties_connexes)
                    self.fusionner_les_parties_connexes(
                        c1, c2, liste_des_parties_connexes)
                    fini = True

    def ajouter_une_arrete_aux_graphes(self):
        test = self.max_arrete - self.nb_darrete_graphes
        if test <= 0:
            return
        for couple in self.liste_des_graphes:
            self.__ajouter_une_arrete_aleatoire(
                couple.matrice, couple.parties_connexes)
        self.nb_darrete_graphes += 1

    def statistique_individuel(self, nombre_aretes):
        self.reset()
        for _ in range(nombre_aretes):
            self.ajouter_une_arrete_aux_graphes()
        moyenne = 0
        for couple in self.liste_des_graphes:
            moyenne += len(couple.parties_connexes)
        moyenne = moyenne / self.nb_graphes
        print('pour', nombre_aretes, 'arêtes, il y a ',
              moyenne, 'parties connexes en moyenne')
        return moyenne

    def statistique_generales_dans_un_fichier(self):
        if not os.path.isdir("Statistiques"):
            os.makedirs("Statistiques")
        t1 = time()
        self.reset()
        fichier = open("Statistiques\Pour "+str(self.nb_graphes) +
                       " graphe à "+str(self.nb_sommets)+" sommet.txt", "w")
        moyenne = 0
        print("Calcul en court...")
        fichier.write("Fichier générer avec GestV2 :\n\n")
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            moyenne = 0
            mini = self.nb_sommets
            maxi = 1
            for couple in self.liste_des_graphes:
                moyenne += len(couple.parties_connexes)
                if len(couple.parties_connexes) < mini:
                    mini = len(couple.parties_connexes)
                if len(couple.parties_connexes) > maxi:
                    maxi = len(couple.parties_connexes)
            moyenne = moyenne / self.nb_graphes
            fichier.write("Pour "+str(self.nb_darrete_graphes) +
                          " arêtes, il y a " + str(round(moyenne, 4)) +
                          " parties connexes en moyenne.")
            fichier.write(" Min = "+str(mini) + " parties connexe.")
            fichier.write(" Max = "+str(maxi) + " parties connexe.\n")
        t2 = time()
        fichier.write("\nTemps d'exécution : "+str(round(t2-t1, 6))+" s.\n")
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print("Fichier créé")
        print()
        fichier.close()

    def statistique_generales_dans_la_console(self):
        t1 = time()
        self.reset()
        moyenne = 0
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            print(self.nb_darrete_graphes)
            moyenne = 0
            mini = self.nb_sommets
            maxi = 1
            for couple in self.liste_des_graphes:
                moyenne += len(couple.parties_connexes)
                if len(couple.parties_connexes) < mini:
                    mini = len(couple.parties_connexes)
                if len(couple.parties_connexes) > maxi:
                    maxi = len(couple.parties_connexes)
            moyenne = moyenne / self.nb_graphes
            print('Pour', self.nb_darrete_graphes, 'arêtes : il y a ',
                  round(moyenne, 4), 'parties connexes en moyenne.')
        t2 = time()
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print()

    def afficher_les_matrices_des_graphes(self):
        for couple in self.liste_des_graphes:
            Algo.afficher_matrice(couple.matrice)


# cette version fait une génération aléatoire très couteuse
# /!\ ce gestionnaire bug
class GestV1:

    def __init__(self, nb_graphes, nb_sommets):
        self.nb_graphes = nb_graphes
        self.nb_sommets = nb_sommets
        self.nb_darrete_graphes = 0
        self.max_arrete = nb_sommets*(nb_sommets-1)//2
        # [[matrice d'adjacence,liste des parties connexes],...] couple
        self.liste_des_graphes = [UpletPourGraphe(
            [[0 for i in range(nb_sommets)] for j in range(nb_sommets)],
            [i for i in range(nb_sommets)],
            [[i] for i in range(nb_sommets)], nb_sommets)
            for k in range(nb_graphes)]

    def reset(self):
        self.nb_darrete_graphes = 0
        self.liste_des_graphes = [UpletPourGraphe(
            [[0 for i in range(self.nb_sommets)] for
             j in range(self.nb_sommets)],
            [i for i in range(self.nb_sommets)],
            [[i] for i in range(self.nb_sommets)], self.nb_sommets)
            for k in range(self.nb_graphes)]

    def fusionner_les_parties_connexes(self, S, T, liste_des_parties_connexe,
                                       liste_representant, nb_partie_connexe):
        i = liste_representant[S]
        j = liste_representant[T]
        if i != j:
            for sommet in liste_des_parties_connexe[j]:
                liste_representant[sommet] = i
                liste_des_parties_connexe[i].append(sommet)
            liste_des_parties_connexe[j] = []
            nb_partie_connexe -= 1

    # matrice d adjacence et liste des parties connexes
    def __ajouter_une_arrete_aleatoire(self, uplet):
        test = self.max_arrete - self.nb_darrete_graphes-1
        try:
            arete = randint(0, test)
        except TypeError:
            return
        token = arete  # compteur pour trouver l'arete
        fini = False
        # rempli une case de la matrice (ajoute une arrête)
        M = uplet.matrice
        for x in range(self.nb_sommets):
            if fini:
                break
            for y in range(x+1, self.nb_sommets):
                if M[x][y] == 0:
                    if token == 0:
                        # arrivée à l'arête à modifier donc je la modifie
                        M[x][y] = 1
                        M[y][x] = 1
                        self.fusionner_les_parties_connexes(x, y,
                                                            uplet.classe_equivalence,  # noqa: E501
                                                            uplet.parties_connexes,    # noqa: E501
                                                            uplet.nb_partie_connexe)  # noqa: E501
                        fini = True
                        break
                    token -= 1

    def ajouter_une_arrete_aux_graphes(self):
        test = self.max_arrete - self.nb_darrete_graphes-1
        if test <= 0:
            return
        for triplet in self.liste_des_graphes:
            self.__ajouter_une_arrete_aleatoire(triplet)
        self.nb_darrete_graphes += 1

    def statistique_individuel(self, nombre_aretes):
        self.reset()
        for _ in range(nombre_aretes):
            self.ajouter_une_arrete_aux_graphes()
        moyenne = 0
        for triplet in self.liste_des_graphes:
            moyenne += triplet.nb_parties_connexes
        moyenne = moyenne / self.nb_graphes
        print('pour', nombre_aretes, 'arêtes, il y a ',
              moyenne, 'parties connexes en moyenne')
        return moyenne


    def statistique_generales_dans_un_fichier(self):
        if not os.path.isdir("Statistiques"):
            os.makedirs("Statistiques")
        t1 = time()
        self.reset()
        fichier = open("Statistiques\Pour "+str(self.nb_graphes) +
                       " graphe à "+str(self.nb_sommets)+" sommet.txt", "w")
        moyenne = 0
        print("Calcul en court...")
        fichier.write("Fichier générer avec GestV1 :\n\n")
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            moyenne = 0
            mini = self.nb_sommets
            maxi = 1
            for triplet in self.liste_des_graphes:
                L = self.longueur(triplet.parties_connexes)
                moyenne += L
                if L < mini:
                    mini = L
                if L > maxi:
                    maxi = L
            moyenne = moyenne / self.nb_graphes
            fichier.write("Pour "+str(self.nb_darrete_graphes) +
                          " arêtes, il y a " + str(round(moyenne, 4)) +
                          " parties connexes en moyenne.")
            fichier.write(" Min = "+str(mini) + " parties connexe.")
            fichier.write(" Max = "+str(maxi) + " parties connexe.\n")
        t2 = time()
        fichier.write("\nTemps d'exécution : "+str(round(t2-t1, 6))+" s.\n")
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print("Fichier créé")
        print()
        fichier.close()

    def statistique_générales_dans_la_console(self):
        t1 = time()
        self.reset()
        moyenne = 0
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            moyenne = 0
            mini = self.nb_sommets
            maxi = 1
            for triplet in self.liste_des_graphes:
                L = self.longueur(triplet.parties_connexes)
                moyenne += L
                if L < mini:
                    mini = L
                if L > maxi:
                    maxi = L
            moyenne = moyenne / self.nb_graphes
            print('Pour', self.nb_darrete_graphes, 'arêtes : il y a ',
                  round(moyenne, 4), 'parties connexes en moyenne.')
        t2 = time()
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print()

    def afficher_les_matrices_des_graphes(self):
        for triplet in self.liste_des_graphes:
            Algo.afficher_matrice(triplet.matrice)
            
    def longueur(self,L) :
        count=0
        for liste in L :
            if liste!=[]:
                count+=1
        return count




# cette version utilise uniquemend réutilse FloydWarshall pour calculer
# les parties connexes à chaques itérations
# à comparer avec la classe GestV1


class GestWarshall:

    def __init__(self, nb_graphes, nb_sommets):
        self.nb_graphes = nb_graphes
        self.nb_sommets = nb_sommets
        self.nb_darrete_graphes = 0
        self.max_arrete = nb_sommets*(nb_sommets-1)//2
        # [[matrice d'adjacence,liste des parties connexes],...] couple
        self.liste_des_graphes = [[[0 for i in range(nb_sommets)] for j
                                   in range(nb_sommets)]
                                  for i in range(nb_graphes)]

    def reset(self):
        self.nb_darrete_graphes = 0
        self.liste_des_graphes = [[[0 for i in range(self.nb_sommets)] for j
                                   in range(self.nb_sommets)] for i
                                  in range(self.nb_graphes)]

    def __ajouter_une_arrete_aleatoire(self, matrice_adjacence):
        test = self.max_arrete - self.nb_darrete_graphes-1
        if test <= 0:
            return
        arete = randint(0, test)
        token = arete  # compteur pour trouver l'arete
        fini = False
        # rempli une case de la matrice (ajoute une arrête)
        M = matrice_adjacence
        for x in range(self.nb_sommets):
            if fini:
                break
            for y in range(x+1, self.nb_sommets):
                if M[x][y] == 0:
                    if token == 0:
                        # arrivée à l'arrête à modifier donc je la modifie
                        M[x][y] = 1
                        M[y][x] = 1
                        fini = True
                        break
                    token -= 1

    def ajouter_une_arrete_aux_graphes(self):
        test = self.max_arrete - self.nb_darrete_graphes-1
        if test <= 0:
            return
        for M in self.liste_des_graphes:
            self.__ajouter_une_arrete_aleatoire(M)
        self.nb_darrete_graphes += 1

    def statistique_generales_dans_la_console(self):
        t1 = time()
        self.reset()
        moyenne = 0
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            liste_nb_parties_connexes = []
            for M in self.liste_des_graphes:
                liste_nb_parties_connexes.append(
                    FloydWarshallPartiesConnexe(M))
            moyenne = 0
            for n in liste_nb_parties_connexes:
                moyenne += n
            moyenne = moyenne / self.nb_graphes
            print('pour', self.nb_darrete_graphes, 'arêtes, il y a ',
                  moyenne, 'parties connexes en moyenne.')
        t2 = time()
        print("\ntemps d'exécution :"+str(t2-t1))
        print()

    def afficher_les_matrices_des_graphes(self):
        for M in self.liste_des_graphes:
            Algo.afficher_matrice(M)


# cette version est comme la V2 mais utilise numpy
class GestV2Numpy:

    def __init__(self, nb_graphes, nb_sommets):
        self.nb_graphes = nb_graphes
        self.nb_sommets = nb_sommets
        self.nb_darrete_graphes = 0
        self.max_arrete = nb_sommets*(nb_sommets-1)//2
        # [[matrice d'adjacence,liste des parties connexes],...] couple
        self.liste_des_graphes = [CoupleGraphePartiesConnexes(
            np.zeros((nb_sommets, nb_sommets), dtype='int'),
            [[i] for i in range(nb_sommets)]) for _
            in range(nb_graphes)]

    def reset(self):
        self.nb_darrete_graphes = 0
        self.liste_des_graphes = [CoupleGraphePartiesConnexes(
            np.zeros((self.nb_sommets, self.nb_sommets), dtype='int'),
            [[i] for i in range(self.nb_sommets)]) for _
            in range(self.nb_graphes)]

    def trouver_index_connexe(self, S, liste_des_parties_connexe):
        for i in range(len(liste_des_parties_connexe)):
            for sommet in liste_des_parties_connexe[i]:
                if sommet == S:
                    return i

    def fusionner_les_parties_connexes(self, i, j, liste_des_parties_connexe):
        if i != j:
            liste_des_parties_connexe[i] = liste_des_parties_connexe[i] + \
                liste_des_parties_connexe[j]
            del liste_des_parties_connexe[j]

    # matrice d adjacence et liste des parties connexes
    def __ajouter_une_arrete_aleatoire(self, matrice_adjacence,
                                       liste_des_parties_connexes):
        limite = self.nb_sommets
        if limite != 0:
            fini = False
            M = matrice_adjacence
            # rempli une case de la matrice (ajoute une arrête
            while not fini:
                x = floor(random()*limite)
                y = floor(random()*limite)
                if M[x][y] == 0 and x != y:
                    M[x][y] = M[y][x] = 1
                    c1 = self.trouver_index_connexe(
                        x, liste_des_parties_connexes)
                    c2 = self.trouver_index_connexe(
                        y, liste_des_parties_connexes)
                    self.fusionner_les_parties_connexes(
                        c1, c2, liste_des_parties_connexes)
                    fini = True

    def ajouter_une_arrete_aux_graphes(self):
        test = self.max_arrete - self.nb_darrete_graphes
        if test <= 0:
            return
        for couple in self.liste_des_graphes:
            self.__ajouter_une_arrete_aleatoire(
                couple.matrice, couple.parties_connexes)
        self.nb_darrete_graphes += 1

    def statistique_individuel(self, nombre_aretes):
        self.reset()
        for _ in range(nombre_aretes):
            self.ajouter_une_arrete_aux_graphes()
        moyenne = 0
        for couple in self.liste_des_graphes:
            moyenne += len(couple.parties_connexes)
        moyenne = moyenne / self.nb_graphes
        print('pour', nombre_aretes, 'arêtes, il y a ',
              moyenne, 'parties connexes en moyenne')
        return moyenne

    def statistique_generales_dans_un_fichier(self):
        if not os.path.isdir("Statistiques"):
            os.makedirs("Statistiques")
        t1 = time()
        self.reset()
        fichier = open("Statistiques\Pour "+str(self.nb_graphes) +
                       " graphe à "+str(self.nb_sommets)+" sommet.txt", "w")
        moyenne = 0
        print("Calcul en court...")
        fichier.write("Fichier générer avec GestV2Numpy :\n\n")
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            moyenne = 0
            mini = self.nb_sommets
            maxi = 1
            for couple in self.liste_des_graphes:
                moyenne += len(couple.parties_connexes)
                if len(couple.parties_connexes) < mini:
                    mini = len(couple.parties_connexes)
                if len(couple.parties_connexes) > maxi:
                    maxi = len(couple.parties_connexes)
            moyenne = moyenne / self.nb_graphes
            fichier.write("Pour "+str(self.nb_darrete_graphes) +
                          " arêtes, il y a " + str(round(moyenne, 4)) +
                          " parties connexes en moyenne.")
            fichier.write(" Min = "+str(mini) + " parties connexe.")
            fichier.write(" Max = "+str(maxi) + " parties connexe.\n")
        t2 = time()
        fichier.write("\nTemps d'exécution : "+str(round(t2-t1, 6))+" s.\n")
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print("Fichier créé")
        print()
        fichier.close()

    def statistique_generales_dans_la_console(self):
        t1 = time()
        self.reset()
        moyenne = 0
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            print(self.nb_darrete_graphes)
            moyenne = 0
            mini = self.nb_sommets
            maxi = 1
            for couple in self.liste_des_graphes:
                moyenne += len(couple.parties_connexes)
                if len(couple.parties_connexes) < mini:
                    mini = len(couple.parties_connexes)
                if len(couple.parties_connexes) > maxi:
                    maxi = len(couple.parties_connexes)
            moyenne = moyenne / self.nb_graphes
            print('Pour', self.nb_darrete_graphes, 'arêtes : il y a ',
                  round(moyenne, 4), 'parties connexes en moyenne.')
        t2 = time()
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print()

    def afficher_les_matrices_des_graphes(self):
        for couple in self.liste_des_graphes:
            print(couple.matrice)
            
            
# test en changeant la façon de calculer les parties connexes
# version la plus rapide actuelle
class GestV2NumpyConex2:

    def __init__(self):
        if not os.path.isdir("Statistiques"):
            os.makedirs("Statistiques")

    def set(self, nb_graphes, nb_sommets):
        self.nb_graphes = nb_graphes
        self.nb_sommets = nb_sommets
        self.nb_darrete_graphes = 0
        self.max_arrete = nb_sommets*(nb_sommets-1)//2
        # [(matrice d'adjacence,liste des parties connexes
        # liste des classe d'équivalence, nb_partie_connexe),...] liste d'uplet
        self.liste_des_graphes = [UpletPourGraphe(
            np.zeros((nb_sommets, nb_sommets)),
            [i for i in range(nb_sommets)],
            [[i] for i in range(nb_sommets)], nb_sommets)
            for _ in range(nb_graphes)]

    def reset(self):
        if self.nb_darrete_graphes != 0 :
            self.nb_darrete_graphes = 0
            self.liste_des_graphes = [UpletPourGraphe(
                np.zeros((self.nb_sommets, self.nb_sommets)),
                [i for i in range(self.nb_sommets)],
                [[i] for i in range(self.nb_sommets)], self.nb_sommets)
                for _ in range(self.nb_graphes)]


    def fus_parties_connexes(self, S1, S2, uplet):
        i = uplet.parties_connexes[S1]
        j = uplet.parties_connexes[S2]
        if i != j:
            for sommet in uplet.classe_equivalence[j]:
                uplet.parties_connexes[sommet] = i
                uplet.classe_equivalence[i].append(sommet)
            uplet.classe_equivalence[j] = []
            uplet.nb_partie_connexe -= 1

    # matrice d adjacence et liste des parties connexes
    def __ajouter_une_arrete_aleatoire(self, uplet):
        """ """
        limite = self.nb_sommets
        if limite != 0:
            fini = False
            M = uplet.matrice
            # rempli une case de la matrice (ajoute une arrête
            while not fini:
                x = floor(random()*limite)
                y = floor(random()*limite)
                if M[x][y] == 0 and x != y:
                    M[x][y] = M[y][x] = 1
                    self.fus_parties_connexes(x, y, uplet)
                    fini = True

    def ajouter_une_arrete_aux_graphes(self):
        test = self.max_arrete - self.nb_darrete_graphes
        if test <= 0:
            return
        for uplet in self.liste_des_graphes:
            self.__ajouter_une_arrete_aleatoire(uplet)
        self.nb_darrete_graphes += 1

    def statistique_individuel(self, nombre_aretes):
        t1 = time()
        self.reset()
        for _ in range(nombre_aretes):
            self.ajouter_une_arrete_aux_graphes()
        moyenne = 0
        for uplet in self.liste_des_graphes :
            moyenne += len(uplet.parties_connexes)
        moyenne = moyenne / self.nb_graphes
        nb_connexe=0
        for uplet in self.liste_des_graphes :
            if uplet.nb_partie_connexe==1 :
                nb_connexe+=1
        print('pour', nombre_aretes, 'arêtes, il y a ',
              moyenne, 'parties connexes en moyenne'," et ",
              round(nb_connexe/self.nb_graphes,3),"% de graphes connexes.")
        t2 = time()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        return moyenne
        
    def connexites_actuelles(self) :
        for i in range(self.nb_graphes)  :
            print("Le graphe " + str(i+1)+ " possède " + 
                str(self.liste_des_graphes[i].nb_partie_connexe)+ 
                " partie connexes.")

    def statistique_generales_dans_un_fichier(self):
        t1 = time()
        if self.nb_darrete_graphes != 0 :
            self.reset()
        fichier = open("Statistiques\Pour "+str(self.nb_graphes) +
                       " graphe à "+str(self.nb_sommets)+" sommet.txt", "w")
        moyenne = 0
        print("Calcul en court...")
        fichier.write("Fichier générer avec GestV2NumpyConex2 :\n\n")
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            moyenne = 0
            mini = self.nb_sommets
            maxi = 1
            for uplet in self.liste_des_graphes:
                L = uplet.nb_partie_connexe
                moyenne += L
                if L < mini:
                    mini = L
                if L > maxi:
                    maxi = L
            moyenne = moyenne / self.nb_graphes
            fichier.write("Pour "+str(self.nb_darrete_graphes) +
                          " arêtes, il y a " + str(round(moyenne, 4)) +
                          " parties connexes en moyenne.")
            fichier.write(" Min = "+str(mini) + " parties connexe.")
            fichier.write(" Max = "+str(maxi) + " parties connexe.\n")
        t2 = time()
        fichier.write("\nTemps d'exécution : "+str(round(t2-t1, 6))+" s.\n")
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print("Fichier créé")
        print()
        fichier.close()
        
    def statistiques1(self):
        self.reset()
        moyenne = 0
        L_moyennes=[]
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            moyenne = 0
            for uplet in self.liste_des_graphes:
                L = uplet.nb_partie_connexe
                moyenne += L
            moyenne = moyenne / self.nb_graphes
            L_moyennes.append(moyenne)
        nb_arrete_final=self.nb_darrete_graphes
        return nb_arrete_final, L_moyennes, 
        # le nombre d'arrête qu'il a fallu ajouté pour que tout les graphes soit connexes
        # la liste du nombre moyen d'arrête

    def statistique_generales_dans_la_console(self):
        t1 = time()
        if self.nb_darrete_graphes != 0 :
            self.reset()
        moyenne = 0
        while moyenne != 1:
            self.ajouter_une_arrete_aux_graphes()
            print(self.nb_darrete_graphes)
            moyenne = 0
            mini = self.nb_sommets
            maxi = 1
            for couple in self.liste_des_graphes:
                moyenne += len(couple.parties_connexes)
                if len(couple.parties_connexes) < mini:
                    mini = len(couple.parties_connexes)
                if len(couple.parties_connexes) > maxi:
                    maxi = len(couple.parties_connexes)
            moyenne = moyenne / self.nb_graphes
            print('Pour', self.nb_darrete_graphes, 'arêtes : il y a ',
                  round(moyenne, 4), 'parties connexes en moyenne.')
        t2 = time()
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print()
        
    def statistiques_des_arretes(self, maxi):
        # cette fonction affiche le nombre final d'arrête à partir duquel
        # tout les graphe sont connexes
        t1=time()
        fichier = open("Statistiques\\Statistiques sur les arrêtes jusqu'à " +
                    str(maxi) + ".txt", "w")
        fichier.write("Fichier générer avec GestV2NumpyConex2 :\n")
        fichier.write("Statistiques sur les arrêtes jusqu'à " + str(maxi) +"\n\n")
        L_s=[]
        L_nb_ar_final=[]
        s=1
        while s < maxi :
            L_s.append(s)
            self.set(100,s)
            L_nb_ar_final.append(self.statistiques1()[0])
            s+=5
        t2 = time()
        fichier.write("\nTemps d'exécution : "+str(round(t2-t1, 6))+" s.\n")
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print("Fichier créé")
        print()
        fichier.close()
        
        plt.plot(L_s,L_nb_ar_final,label="Nb d'arrêtes finale",marker='o')
        plt.legend()
        plt.show()

    def statistiques_de_connexite1(self, maxi, N):
        # cette fonction fait varier K dans la formule
        # nb max d'arrete a essayer, N sommet dans les graphes
        t1=time()
        self.set(200,N)
        
        L_K=[]
        L_prop_graph_connex=[]
        K=1
        while K < maxi :
            ar=floor(N*(K+log(N))/2)
            L_K.append(K)
            for i in range(ar):
                self.ajouter_une_arrete_aux_graphes()
            count=0
            for uplet in self.liste_des_graphes:
                if uplet.nb_partie_connexe == 1:
                    count+=1
            L_prop_graph_connex.append(count/self.nb_graphes)
            K+=1
            
        L_th=[]
        for K in L_K:
            L_th.append(exp(-exp(-K)))
            
        t2 = time()
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print()
        
        plt.plot(L_K,L_prop_graph_connex,label="Valeur mesurés",marker='o')
        plt.plot(L_K,L_th,label="Valeurs théoriques",marker='o')
        plt.legend()
        plt.show()
        
    
    def statistiques_de_connexite2(self, maxi, K, pas):
        # cette fonction fait varier N dans la formule
        # nb max d'arrete a essayer, K constante de la formule
        t1=time()
        L_N=[]
        L_prop_graph_connex=[]
        for N in range(1,maxi,pas) :
            self.set(100,N)
            ar=floor(N*(K+log(N))/2)
            L_N.append(N)
            for _ in range(ar) :
                self.ajouter_une_arrete_aux_graphes()
            count=0
            for uplet in self.liste_des_graphes:
                if uplet.nb_partie_connexe == 1:
                    count+=1
            L_prop_graph_connex.append(count/self.nb_graphes)                
        
        L_th=[]
        for N in L_N:
            L_th.append(exp(-exp(-K)))
            
        t2 = time()
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print()
        
        plt.plot(L_N,L_prop_graph_connex,label="Valeur mesurés",marker='o')
        plt.plot(L_N,L_th,label="Valeurs théoriques")
        plt.legend()
        plt.show()
        
    def statistiques_de_connexite3(self, ar_max) :
        # cette fonction fait varier le nombre d'arête jusqu'à ar_max
        t1=time()
        self.reset()
        L_A=[]
        L_prop_graph_connex=[]
        for A in range(0,ar_max,5):
            self.ajouter_une_arrete_aux_graphes()
            count = 0
            for uplet in self.liste_des_graphes :
                if uplet.nb_partie_connexe==1:
                    count += 1
            moyenne = count / self.nb_graphes
            L_A.append(A)
            L_prop_graph_connex.append(moyenne)
            if moyenne == 1:
                break
        t2 = time()
        print()
        print("Temps d'exécution : "+str(round(t2-t1, 6))+" s")
        print()
        
        plt.plot(L_A,L_prop_graph_connex,label="Proportion de graphes connexes",marker='o')
        plt.legend()
        plt.show()

    def afficher_les_matrices_des_graphes(self):
        for couple in self.liste_des_graphes:
            print(couple.matrice)

## code de test rapide
# G=GestV2NumpyConex2()
# G.statistiques_des_arretes(150)