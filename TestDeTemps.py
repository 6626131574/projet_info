from math import floor
from random import randint, random
from time import time, sleep
import matplotlib.pyplot as plt
import Algo
from threading import Thread


def tests_des_temps_de_calcul(N):

    # temps de l'appel range(N)
    tps_range = temps_range(N)
    print("Temps pour créer une boucle for sur un range de " +
          Algo.decoupe_numero(str(N)) + " itérations : ")
    print("tps_range==0 : ", tps_range==0, "\n")

    print("Les résultats qui suivent sont des moyennes sur " +
          str(N)+" opérations :\n")
    # vérification que python crée les boucle de façon intelligente
    n = 1000000000000*N
    tps_range2 = temps_range(n)
    print("Temps pour créer une boucle for sur un range de " +
          Algo.decoupe_numero(str(n)) + " itérations : " +
          str(tps_range2) + "s")

    # temps de parcours d'une boucle for
    tps_parcours = temps_parcours(N)
    print("Temps pour parcourir une boucle for sur un range de " +
          Algo.decoupe_numero(str(N)) + " itérations : " +
          str(tps_parcours) + "s\n")
    
    # le temps d'une affectation (associer au parcours de la boucle)
    tps_affectation = temps_affectation(N)
    print("Temps d'une affectation :                           {0:.5e}".format(
        tps_affectation) + "s")

    # le temps d'une addition
    tps_daddition = temps_addition(N,tps_affectation)
    print("Temps d'une addition :                              {0:.5e}".format(
        tps_daddition) + "s")

    # le temps d'une multiplication
    tps_multiplication = temps_multiplication(N,tps_affectation)
    print("Temps d'une multiplication :                        {0:.5e}".format(
        tps_multiplication) + "s")

    # le temps d'une division
    tps_division = temps_division(N,tps_affectation)
    print("Temps d'une division :                              {0:.5e}".format(
        tps_division) + "s")

    # le temps d'une soustraction
    tps_soustraction = temps_soustraction(N,tps_affectation)
    print("Temps d'une soustraction :                          {0:.5e}".format(
        tps_soustraction) + "s\n")

    # le temps d'une combinaison des 4 opérations
    tps_combine = temps_4_operations(N,tps_affectation)
    print("Temps pour la somme des 4 temps :                   {0:.5e}".format(
        tps_daddition + tps_multiplication +
        tps_division + tps_soustraction) + "s")
    print("Temps pour une combinaison des 4 opérations :       {0:.5e}".format(
        tps_combine) + "s\n")

    # le temps pour tirer un entier au hasard
    tps_random = temps_entier_au_hasard(N,tps_affectation)
    print("Temps du tirage d'un entier aléatoire :             {0:.5e}".format(
        tps_random) + "s")

    # le temps pour tirer un entier au hasard manuellement
    tps_random2 = temps_entier_au_hasard_manuel(N,tps_affectation)
    print("Temps du tirage d'un entier aléatoire à la main :   {0:.5e}".format(
        tps_random2) + "s")
        
    # le temps pour tirer entre 0 et 1 au hasard
    tps_random3 = temps_decimal_au_hasard(N,tps_affectation)
    print("Temps du tirage d'un décimal aléatoire' :           {0:.5e}".format(
        tps_random3) + "s")


    # test du temps d'une ligne de code du gestionnaire
    tps_ligne1 = temps_ligne_code_du_gestionnaire(N,tps_affectation)
    print("Temps du calcul \"x=floor(random()*2356)\" :          {0:.5e}".format(
        tps_ligne1) + "s")

        

def tests_des_temps_de_calcul_graphique(N):
    # en dessous de 50000 les valeur n'ont pas de sens utilisable
    K=50000
    thread1=Afficheur()
    Lx=[]
    Ly=[]
    while K<N :
        Ly.append(temps_affectation(K))
        Lx.append(K)
        K=floor(K*1.2)
    thread1.ajout_courbe(Lx,Ly,"temps_affectation")
    
    K=50000
    Lx=[]
    Ly=[]
    while K<N :
        tps_affect=temps_affectation(K)
        Ly.append(temps_addition(K,tps_affect))
        Lx.append(K)
        K=floor(K*1.3)
    thread1.ajout_courbe(Lx,Ly,"temps_addition")
    #thread1.start()
    thread1.affiche_courbe()

    
# remarque le temps de l'appel a un range est tellement courts qu'il affiche 0s
def temps_range(N):
    # temps de l'appel range(N)
    t1 = time()
    for _ in range(N):
        if(_ == 2):
            break
    t2 = time()
    return t2-t1


def temps_parcours(N):
    # temps de parcours d'une boucle for
    t1 = time()
    y = 80
    for i in range(N):
        if(i == 80):
            y = y**y  # noqa: F841
    t2 = time()
    return t2-t1

    # rmq : je n'arrive pas a disocier le temps d'une affectation
    # avec celui du parcours de la boucle for
    # mais ici tout les test font 1 affectation dans chaque tour de
    # boucle donc les calculs finaux sont correct

def temps_affectation(N):
    # le temps d'une affectation
    t1 = time()
    for i in range(N):
        x = i
    t2 = time()
    return (t2-t1)/N


def temps_addition(N,tps_affectation):
    # le temps d'une addition
    t1 = time()
    for i in range(N):
        x = i+i
    t2 = time()
    return (t2-t1-tps_affectation*N)/N


def temps_multiplication(N,tps_affectation):
    # le temps d'une multiplication
    t1 = time()
    for i in range(N):
        x = i*i
    t2 = time()
    return (t2-t1-tps_affectation*N)/N


def temps_division(N,tps_affectation):
    # le temps d'une division
    t1 = time()
    for i in range(N):
        x = i/5
    t2 = time()
    return (t2-t1-tps_affectation*N)/N


def temps_soustraction(N,tps_affectation):
    # le temps d'une soustraction
    t1 = time()
    for i in range(N):
        x = i-2541
    t2 = time()
    return (t2-t1-tps_affectation*N)/N


def temps_4_operations(N,tps_affectation):
    # le temps d'une combinaison des 4 opérations
    t1 = time()
    for i in range(N):
        x = (i-2541)*i/(i+8542)
    t2 = time()
    return (t2-t1-tps_affectation*N)/N


def temps_entier_au_hasard(N,tps_affectation):
    # le temps pour tirer un entier au hasard
    t1 = time()
    for i in range(N):
        x = randint(0, 500000)
    t2 = time()
    return (t2-t1-tps_affectation*N)/N


def temps_entier_au_hasard_manuel(N,tps_affectation):
    # le temps pour tirer un entier au hasard à la main
    t1 = time()
    for i in range(N):
        x = floor(random()*2356)
    t2 = time()
    return (t2-t1-tps_affectation*N)/N
    

def temps_decimal_au_hasard(N,tps_affectation):
    # le temps pour tirer un décimal au hasard
    t1 = time()
    for i in range(N):
        x = random()
    t2 = time()
    return (t2-t1-tps_affectation*N)/N


def temps_ligne_code_du_gestionnaire(N,tps_affectation):
    # test du temps d'une ligne de code du gestionnaire
    t1 = time()
    for i in range(N):
        x = floor(random()*2356)  # noqa: F841
    t2 = time()
    return (t2-t1-tps_affectation*N)/N

   
## c'est classe ça    
class Afficheur(Thread):

    """Thread chargé d'afficher un graphique indépendent de l'exécution du reste du code."""

    def __init__(self):
        Thread.__init__(self)


    def ajout_courbe(self,Lx, Ly,texte):
        plt.plot(Lx,Ly,label=texte,marker='o')


    def run(self):
        """Code à exécuter pendant l'exécution du thread."""
        pass
    
    def affiche_courbe(self):
        plt.legend()
        plt.show()