#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb  3 11:14:36 2019

@author: cecilepetitet
"""
import Algo


def FloydWarshallConnexe(M):  # on entre la matrice d'adjacence
    n = len(M[0])  # nombre de sommets
    puissance = 0
    matrice = M
    reponse = False
    while not reponse and puissance < n:
        # à la place de visiter la matrice
        # on ne va visiter que la liste des chaines non existentes encore
        for i in range(n-1):
            for j in range(i+1, n):
                if M[i][j] == 0:
                    if M[i][puissance] == 1 and M[puissance][j] == 1:
                        M[i][j] = M[j][i] = 1
        puissance += 1
        if matrice == [[1 for _ in range(n)] for _ in range(n)]:
            reponse = True

    if reponse:
        return (' le graphe est connexe')

    return (' le graphe n\'est pas connexe')


def FloydWarshallConnexeFaitMaison(M):  # on entre la matrice d'adjacence
    n = len(M[0])  # nombre de sommets
    en_etude = []
    for i in range(n-1):
        # on regarde combien il y a de 0.
        # Complexité en n(n-1)/2,
        # partie triangulaire supérieure de la matrice sans diagonale
        for j in range(i+1, n):
            if M[i][j] == 0:
                en_etude.append((i, j))

    puissance = 0

    # à la place de visiter la matrice
    # on ne va visiter que la liste des chaines non existentes encore
    while en_etude != [] and puissance < n:

        for idx, (x, y) in enumerate(en_etude):
            # ça veut dire que la chaine existe
            if ((min(x, puissance), max(x, puissance)) not in en_etude
                    and (min(y, puissance), max(y, puissance)) not in en_etude):
                en_etude[idx] = (-1, -1)
            # le sinon est :
            # la chaine n'existe pas donc on laisse le couple dans la liste
        k = 0
        while k < len(en_etude):
            if en_etude[k] == (-1, -1):
                del en_etude[k]
            else:
                k += 1
        puissance += 1

    if en_etude == []:
        return (' le graphe est connexe')

    return (' le graphe n\'est pas connexe')


def test():
    print("Test de l'algo FloydWarshallConnexe")
    M = [
        [1, 1, 0, 1, 0],
        [1, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [1, 0, 0, 1, 0],
        [0, 0, 0, 0, 1]]
    Algo.afficher_matrice(M)

    print("Le graphe est non connexe et pour l'algo", FloydWarshallConnexe(M))

    M = [
        [1, 1, 0, 1, 0],
        [1, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [1, 0, 0, 1, 1],
        [0, 0, 0, 1, 1]]
    Algo.afficher_matrice(M)

    print("Le graphe est connexe et pour l'algo", FloydWarshallConnexe(M))
    print()
    print("Test de l'algo FloydWarshallConnexe")
    M = [
        [1, 1, 0, 1, 0],
        [1, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [1, 0, 0, 1, 0],
        [0, 0, 0, 0, 1]]
    Algo.afficher_matrice(M)

    print("Le graphe est non connexe et pour l'algo",
          FloydWarshallConnexeFaitMaison(M))

    M = [
        [1, 1, 0, 1, 0],
        [1, 1, 1, 0, 0],
        [0, 1, 1, 0, 0],
        [1, 0, 0, 1, 1],
        [0, 0, 0, 1, 1]]
    Algo.afficher_matrice(M)

    print("Le graphe est connexe et pour l'algo",
          FloydWarshallConnexeFaitMaison(M))
