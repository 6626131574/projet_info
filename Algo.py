from math import floor
from random import randint, random
from time import time


def est_un_graphe_par_liste_dadjacence(graphe):
    return True


def ExplorerRecursif(graphe, dejaVisite, sommet, partie_connexe):
    dejaVisite[sommet] = True
    partie_connexe.append(sommet)
    print("sommet "+str(sommet)+" visité")
    for S in graphe[sommet]:
        if not dejaVisite[S]:
            ExplorerRecursif(graphe, dejaVisite, S, partie_connexe)


def choisir_un_sommet_de_depart(dejaVisite):
    choix = -1
    for i in range(len(dejaVisite)):
        if not dejaVisite[i]:
            choix = i
            break
    return choix


# le graphe doit être donné par liste d'adjacence
def TestConnexiteEnProfondeur(graphe):
    if est_un_graphe_par_liste_dadjacence(graphe):
        # False veux dire que le sommet n'a pas encore été visité
        dejaVisite = [False for i in range(len(graphe))]
        ListeDesPartiesConnexes = []
        SommetDeDepart = choisir_un_sommet_de_depart(dejaVisite)
        while SommetDeDepart > -1:
            partie_connexe = []
            ExplorerRecursif(graphe, dejaVisite,
                             SommetDeDepart, partie_connexe)
            ListeDesPartiesConnexes.append(partie_connexe)
            SommetDeDepart = choisir_un_sommet_de_depart(dejaVisite)
        return ListeDesPartiesConnexes


# génère la liste des voisins d'un graphe de façon aléatoire
def genere_un_graphe_aleatoire(nb_sommets, nb_arrete_demande):
    index_arrete_max = nb_sommets*(nb_sommets-1)/2-1
    nb_arrete = min(nb_arrete_demande, index_arrete_max+1)
    M = [[0 for j in range(nb_sommets)] for i in range(nb_sommets)]
    for i in range(nb_arrete):
        arete = randint(0, index_arrete_max-i)
        token = arete
        fini = False
        # rempli une case de la matrice (ajoute une arrête)
        for x in range(nb_sommets):
            if fini:
                break
            for y in range(x+1, nb_sommets):
                if M[x][y] == 0:
                    if token == 0:
                        M[x][y] = 1
                        M[y][x] = 1
                        fini = True
                        break
                    token -= 1

    return M

# génère la liste des voisins d'un graphe de façon aléatoire version numpy


def genere_un_graphe_aleatoire_numpy(nb_sommets, nb_arrete_demande):
    index_arrete_max = nb_sommets*(nb_sommets-1)/2-1
    nb_arrete = min(nb_arrete_demande, index_arrete_max+1)
    M = np.zeros((nb_sommets, nb_sommets), dtype='int')
    for i in range(nb_arrete):
        arete = randint(0, index_arrete_max-i)
        token = arete
        fini = False
        # rempli une case de la matrice (ajoute une arrête)
        for x in range(nb_sommets):
            if fini:
                break
            for y in range(x+1, nb_sommets):
                if M[x][y] == 0:
                    if token == 0:
                        M[x][y] = 1
                        M[y][x] = 1
                        fini = True
                        break
                    token -= 1

    return M


# (liste des voisins,liste des parties connexes)
def ajouter_une_arrete_aleatoire(L, LC):
    print("a")


def afficher_matrice(M):
    print()
    for i in range(len(M)):
        print(M[i])
    print()


def afficher_liste_dadjacence(L):
    print()
    for i in range(len(L)):
        print(str(i)+" : "+str(L[i]))
    print()


def convertir_vers_liste_des_voisins(M):
    L = [[] for i in range(len(M))]
    for x in range(len(M)):
        for y in range(len(M)):
            if M[x][y] == 1:
                L[x].append(y)
    return L


def convertir_vers_matrice_dadjacence(L):
    M = [[0 for i in range(len(L))] for i in range(len(L))]
    for x in range(len(L)):
        for y in L[x]:
            M[x][y] = 1
    return M


def convertir_vers_matrice_dadjacence_numpy(L):
    M = np.zeros((len(L), len(L)), dtype='int')
    for x in range(len(L)):
        for y in L[x]:
            M[x][y] = 1
    return M


def decoupe_numero(T):
    resultat = ""
    r = len(T) % 3
    for i in range(r):
        resultat += T[i]
    resultat += " "
    token = 0
    for i in range(r, len(T)):
        resultat += T[i]
        token += 1
        if token == 3:
            resultat += " "
            token = 0
    return resultat
