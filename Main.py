import Algo
import GUI
import Gestionnaires
import TestDeTemps as tdt

exit = False
print("Taper \"help\" pour avoir la liste des commandes.")
F = 0
G = 0


def help_du_menu():
    print("\n--------------- help -----------------")
    print("exit : pour fermer le programme.")
    print("1 : pour ouvrir une fenêtre graphique.")
    print("21 : simulateur des graphes en console" +
          " avec le gestionnaire Warshall.")
    print("22 : simulateur des graphes en console" +
          " qui gère les parties connexes progressivement" +
          " et une amélioration de la génération aléatoire.")
    print("23 : simulateur des graphes en console" +
          " avec l'utilisation de numpy pour gérer les matrices.")
    # print("24 : simulateur des graphes en console" +
    #       " qui gère les parties connexes progressivement" +
    #       " mais dont la génération aléatoire été moins performante.")
    print("25 : simulateur des graphes en console" +
          " identique au 23 mais avec une gestion des partie connexe" +
          " plus rapide.")
    print("31 ou tests : pour lancer les tests des temps de calcul " +
          "des opérations de bases sur l'ordinateur.")
    print("32 : pour lancer les tests des temps de calcul " +
          "avec un affichage graphique.")
    print("--------------------------------")


def help_gestionnaires(G):
    print("\n--------------------------------")
    print("help : Pour afficher la liste des commandes.")
    print("exit : pour fermer le programme")
    print("menu : pour revenir au menu principal")
    print("1 : Pour ajouter une arrête aleatoire à tout les graphes.")
    print("2 : pour afficher la matrice d'adjacence de tout les graphes.")
    print("3 ou stats simple : pour calculer des statistiques " +
          "sur la connexité de vos ", G.nb_graphes, " graphes à ",
          G.nb_sommets, " sommets.")
    print("4 ou stats totales1 : pour calculer des statistiques sur " +
          "la connexité des graphes pour tout nombre d'arrête et afficher " +
          "les résultats dans la console.")
    print("5 ou stats totales2 : pour calculer des statistiques sur " +
          "la connexité des graphes pour tout nombre d'arrête et stocker " +
          "les résultats dans un fichier.")
    print("6 : pour modifier le nombre de graphes et de sommets gérer par le" +
          " gestionnaire.")
    print("reset : pour recréer des graphes vierges")
    print("--------------------------------")
    

def help_gestionnaires_final(G):
    print("\n--------------------------------")
    print("help : Pour afficher la liste des commandes.")
    print("exit : pour fermer le programme")
    print("menu : pour revenir au menu principal")
    print("1 : Pour ajouter une arrête aleatoire à tout les graphes.")
    print("2 : pour afficher la matrice d'adjacence de tout les graphes.")
    print("31 ou stats simple : pour calculer des statistiques " +
          "sur la connexité de vos ", G.nb_graphes, " graphes à ",
          G.nb_sommets, " sommets.")
    print("32 : pour afficher le nombre de parties connexes de chaque graphe.")
    print("4 ou stats totales1 : pour calculer des statistiques sur " +
          "la connexité des graphes pour tout nombre d'arrête et afficher " +
          "les résultats dans la console.")
    print("5 ou stats totales2 : pour calculer des statistiques sur " +
          "la connexité des graphes pour tout nombre d'arrête et stocker " +
          "les résultats dans un fichier.")
    print("6 : pour modifier le nombre de graphes et de sommets gérer par le" +
          " gestionnaire.")
    print("7 : lance un test qui affiche le nombre d'arrête qu'il faut"+
          " donner aux graphes pour qu'ils soient tous connexes."+
          " (statistiques mené sur 100 graphes.")
    print("8 : lance un test qui affiche la proportion de graphe conexe en"+
          " comparaison avec la valeur théorique en fonction de K."+
          " (stats réalisé sur 200 graphes")
    print("9 : lance un test qui affiche la proportion de graphes connexes"+
          " en fonction du nombre de sommet, et la compare à la"+
          " valeur théorique asymptotique.")
    print("10 : lance un test qui affiche la proportion de graphe connexe"+
            " en fonction du nombre d'arêtes."+ 
            " (stats sur le nombre de graphes atuel.)")
    print("reset : pour recréer des graphes vierges")
    print("--------------------------------")


def help_du_gestionnaire_warshall(G):
    print("\n-------------- help --------------")
    print("help : Pour afficher la liste des commandes.")
    print("exit : pour fermer le programme")
    print("menu : pour revenir au menu principal")
    print("1 : Pour ajouter une arrête aleatoire à tout les graphes.")
    print("2 : pour afficher la matrice d'adjacence de tout les graphes.")
    print("4 ou stats totales1 : pour calculer des statistiques sur " +
          "la connexité des graphes pour tout nombre d'arrête et" +
          " afficher les résultats dans la console.")
    print("reset : pour recréer des graphes vierges")
    print("----------------------------------")


def help_de_la_fenetre():
    print("\n--------------------------------")
    print("Clic gauche pour créer un sommet de graphe.")
    print("Clic molette pour créer des arêtes.")
    print("Il faut fermer la fenêtre pour récupérer la main sur " +
          "le programme principal.")
    print("--------------------------------")


help_du_menu()
while(not exit):
    print("\n----- Menu principal -----\nEntrez une commande :")
    commande = input()
    if commande == "exit" or commande == "quit":
        exit = True
    elif commande == "0":
        exit = True
    elif commande == "help":
        help_du_menu()
    elif commande == "1":
        help_de_la_fenetre()
        F = GUI.Fenetre()
    elif commande == "21":
        nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
        if nb_graphes != -1:
            G = Gestionnaires.GestWarshall(nb_graphes, nb_sommets)
            print("Taper \"help\" pour avoir la liste des commandes.")
            menu = False
        else :
            menu = True
        while(not exit and not menu):
            print("\n----- Gestionnaire 21 -----\nEntrez une commande :")
            commande = input()
            if commande == "help":
                help_du_gestionnaire_warshall(G)
            elif commande == "exit":
                exit = True
            elif commande == "menu":
                menu = True
            elif commande == "1":
                G.ajouter_une_arrete_aux_graphes()
            elif commande == "2":
                G.afficher_les_matrices_des_graphes()
            elif commande == "stats totales1" or commande == "4":
                G.statistique_generales_dans_la_console()
            elif commande == "6":
                nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
                G=Gestionnaires.GestWarshall(nb_graphes, nb_sommets)
            elif commande == "reset":
                G.reset()
            else:
                print("\""+commande+"\" n'est pas une commande reconnue.")
    elif commande == "22":
        nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
        if nb_graphes != -1:
            G = Gestionnaires.GestV2(nb_graphes, nb_sommets)
            print("Taper \"help\" pour avoir la liste des commandes.")
            menu = False
        else :
            menu = True
        while(not exit and not menu):
            print("\n----- Gestionnaire 22 -----\nEntrez une commande :")
            commande = input()
            if commande == "help":
                help_gestionnaires(G)
            elif commande == "exit":
                exit = True
            elif commande == "menu":
                menu = True
            elif commande == "1":
                G.ajouter_une_arrete_aux_graphes()
            elif commande == "2":
                G.afficher_les_matrices_des_graphes()
            elif commande == "3" or commande == "stats simple":
                print("Statistique pour combien d'aretes ?")
                n = input()
                G.statistique_individuel(int(n))
            elif commande == "4" or commande == "stats totales1":
                G.statistique_générales_dans_la_console()
            elif commande == "5" or commande == "stats totales2":
                G.statistique_generales_dans_un_fichier()
            elif commande == "6":
                nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
                G=Gestionnaires.GestV2(nb_graphes, nb_sommets)
            elif commande == "reset":
                G.reset()
            else:
                print("\""+commande+"\" n'est pas une commande reconnue.")
    elif commande == "23":
        nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
        if nb_graphes != -1:
            G = Gestionnaires.GestV2Numpy(nb_graphes, nb_sommets)
            print("Taper \"help\" pour avoir la liste des commandes.")
            menu = False
        else :
            menu = True
        while(not exit and not menu):
            print("\n----- Gestionnaire 23 -----\nEntrez une commande :")
            commande = input()
            if commande == "help":
                help_gestionnaires(G)
            elif commande == "exit":
                exit = True
            elif commande == "menu":
                menu = True
            elif commande == "1":
                G.ajouter_une_arrete_aux_graphes()
            elif commande == "2":
                G.afficher_les_matrices_des_graphes()
            elif commande == "3" or commande == "stats simple":
                print("Statistique pour combien d'aretes ?")
                n = input()
                G.statistique_individuel(int(n))
            elif commande == "4" or commande == "stats totales1":
                G.statistique_générales_dans_la_console()
            elif commande == "5" or commande == "stats totales2":
                G.statistique_generales_dans_un_fichier()
            elif commande == "6":
                nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
                G=Gestionnaires.GestV2Numpy(nb_graphes, nb_sommets)
            elif commande == "reset":
                G.reset()
            else:
                print("\""+commande+"\" n'est pas une commande reconnue.")
    # ce gestionnaire bug
    # elif commande == "24":
    #     nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
    #     if nb_graphes != -1:
    #         G = Gestionnaires.GestV1(nb_graphes, nb_sommets)
    #         print("Taper \"help\" pour avoir la liste des commandes.")
    #         menu = False
    #     else :
    #         menu = True
    #     while(not exit and not menu):
    #         print("\n----- Gestionnaire 24 -----\nEntrez une commande :")
    #         commande = input()
    #         if commande == "help":
    #             help_gestionnaires(G)
    #         elif commande == "exit":
    #             exit = True
    #         elif commande == "menu":
    #             menu = True
    #         elif commande == "1":
    #             G.ajouter_une_arrete_aux_graphes()
    #         elif commande == "2":
    #             G.afficher_les_matrices_des_graphes()
    #         elif commande == "3" or commande == "stats simple":
    #             print("Statistique pour combien d'aretes ?")
    #             n = input()
    #             G.statistique_individuel(int(n))
    #         elif commande == "4" or commande == "stats totales1":
    #             G.statistique_generales_dans_la_console()
    #         elif commande == "5" or commande == "stats totales2":
    #             G.statistique_generales_dans_un_fichier()
    #         elif commande == "6":
    #             nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
    #             G=Gestionnaires.GestV1(nb_graphes,
    #                                                         nb_sommets)
    #         elif commande == "reset":
    #             G.reset()
    #         else:
    #             print("\""+commande+"\" n'est pas une commande reconnue.")
    elif commande == "25":
        nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
        if nb_graphes != -1:
            G = Gestionnaires.GestV2NumpyConex2()
            G.set(nb_graphes, nb_sommets)
            print("Taper \"help\" pour avoir la liste des commandes.")
            menu = False
        else :
            menu = True
        while(not exit and not menu):
            print("\n----- Gestionnaire 25 -----\nEntrez une commande :")
            commande = input()
            if commande == "help":
                help_gestionnaires_final(G)
            elif commande == "exit":
                exit = True
            elif commande == "menu":
                menu = True
            elif commande == "1":
                G.ajouter_une_arrete_aux_graphes()
            elif commande == "2":
                G.afficher_les_matrices_des_graphes()
            elif commande == "31" or commande == "stats simple":
                print("Statistique pour combien d'aretes ?")
                n = input()
                G.statistique_individuel(int(n))
            elif commande == "32" :
                G.connexites_actuelles()
            elif commande == "4" or commande == "stats totales1":
                G.statistique_générales_dans_la_console()
            elif commande == "5" or commande == "stats totales2":
                G.statistique_generales_dans_un_fichier()
            elif commande == "6":
                nb_graphes, nb_sommets = Gestionnaires.lire_parametre()
                G.set(nb_graphes, nb_sommets)
            elif commande == "7":
                print("Jusqu'à combien de sommets ?")
                nb_sommets = input()
                try :
                    nb_sommets=int(nb_sommets)
                    G.statistiques_des_arretes(nb_sommets)
                except TypeError:
                    print("Erreur : Il faut saisir un nombre entier.")
            elif commande == "8":
                print("Valeur max de K ?")
                K_maxi = input()
                print("Nombre de sommet ?")
                nb_sommets=input()
                try :
                    K_maxi=int(K_maxi)
                    nb_sommets=int(nb_sommets)
                    G.statistiques_de_connexite1(K_maxi,nb_sommets)
                except TypeError:
                     print("Erreur : Il faut saisir un nombre entier.")
            elif commande == "9":
                print("Nombre de sommet max (valeur de N) ?")
                nb_sommets = input()
                print("Valeur de K ?")
                K = input()
                print("Valeur du pas ?")
                pas = input()
                try :
                    nb_sommets=int(nb_sommets)
                    K=int(K)
                    pas=int(pas)
                    G.statistiques_de_connexite2(nb_sommets,K,pas)
                except TypeError:
                     print("Erreur : Il faut saisir un nombre entier.")
            elif commande == "10":
                print("Valeur d'arêtes max ?")
                ar_max = input()
                try :
                    ar_max=int(ar_max)
                    G.statistiques_de_connexite3(ar_max)
                except TypeError:
                     print("Erreur : Il faut saisir un nombre entier.")
            elif commande == "reset":
                G.reset()
            else:
                print("\""+commande+"\" n'est pas une commande reconnue.")
    elif commande == "31" or commande == "tests":
        print("------------")
        print("Début des tests...\n")
        tdt.tests_des_temps_de_calcul(10000000)
        print("\nFin des tests")
        print("------------")
    elif commande == "32":
        print("------------")
        print("Début des tests...\n")
        tdt.tests_des_temps_de_calcul_graphique(10000000)
        print("\nFin des tests.")
        print("------------")
    else:
        print("\""+commande+"\" n'est pas une commande reconnue.")
